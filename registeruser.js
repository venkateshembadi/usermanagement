function registerUser() {

    var name = document.getElementById("username").value;
    var mno = document.getElementById("mno").value;
    var sapId = document.getElementById("sapId").value;
    var email = document.getElementById("email").value;
    var gender = document.getElementById("gender").nodeValue;
    var gender;
    var ele = document.getElementsByName('gender');
    for (i = 0; i < ele.length; i++) {
        if (ele[i].checked)
            gender = ele[i].value + " ";
    }
    var obj = { name: name, mno: mno, sapId: sapId, email: email, gender: gender };
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var pattern="[A-Za-z0-9._%+-]*(@dbs.com|@hcl.com)";
    if (name == "") {
        window.alert("Please enter your name.");
        name.focus();
        return false;
    }
    
    else if (email == "" || !email.match(mailformat) || !email.match(pattern)) {
        window.alert("Please enter a valid e-mail address.");
        email.focus();
        return false;
    }
    
    else if (sapId == "") {
        window.alert("Please enter a valid sapId.");
        sapId.focus();
        return false;
    }
    else if (mno == "" || mno.length !=10) {
        window.alert("Please enter a valid Mobile");
        mno.focus();
        return false;
    }
    

    var httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }
    httpRequest.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 201) {
            alert("User Details Added successfully");
            getData();
        }
    }
    httpRequest.open("post", "http://localhost:3000/users", true);
    httpRequest.setRequestHeader("Content-type", "application/json");
    httpRequest.send(JSON.stringify(obj));
}


function crateUser() {

    var body = document.getElementsByTagName('body')[0];
    //User Name
    var username = document.createElement("input");
    var userLabel = document.createElement("LABEL");
    userLabel.innerHTML="User Name";
    username.setAttribute('type', "text");
    username.setAttribute('name', "username");
    username.setAttribute('id', "username");
    username.setAttribute('placeholder', "Enter Name");
   
    
    var br1 = document.createElement("br");
    username.appendChild(br1);
    //SapId
    var sapId = document.createElement("input");
    sapId.setAttribute('type', "text");
    sapId.setAttribute('name', "sapId");
    sapId.setAttribute('id', "sapId");
    sapId.setAttribute('placeholder', "Enter sapId");

    var br2 = document.createElement("br");


    //Mobile Number
    var br3 = document.createElement("br");
    var mno = document.createElement("input");
    mno.setAttribute('type', "number");
    mno.setAttribute('name', "mno");
    mno.setAttribute('id', "mno");
    mno.setAttribute('placeholder', "Enter Mobile Number");

    //email
    var email = document.createElement("input");
    email.setAttribute('type', "email");
    email.setAttribute('name', "email");
    email.setAttribute('id', "email");
    email.setAttribute('placeholder', "Enter Email");
    //Gener
    var gender = document.createElement("input");
    gender.setAttribute('type', "radio");
    gender.setAttribute('name', "gender");
    gender.setAttribute('id', "gender");
    gender.setAttribute('value', "male");
    gender.setAttribute('placeholder', "Enter gender");

    var gender1 = document.createElement("input");
    gender1.setAttribute('type', "radio");
    gender1.setAttribute('name', "gender");
    gender1.setAttribute('id', "gender");
    gender1.setAttribute('value', "female");
    gender1.setAttribute('placeholder', "Enter gender");

    var registerButton = document.createElement("button");

    registerButton.appendChild(document.createTextNode("Add User"))
    registerButton.addEventListener('click', function () {

        registerUser();
        getData();
    });

    body.appendChild(username);
    body.appendChild(sapId);
    body.appendChild(mno);
    body.appendChild(email);
    body.appendChild(gender);
    body.appendChild(gender1);
    body.appendChild(registerButton);


}

function getData() {
    var httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }
    httpRequest.onreadystatechange = function () {
        if (this.readyState === 4 && this.status == 200) {

            var tableEl = document.getElementsByTagName('table');
            if (tableEl[0] !== undefined) {
                tableEl[0].remove()
            }
            var table = document.createElement("table");

            var tbody = document.createElement("tbody");

            var thead = document.createElement("thead");

            var tr = document.createElement("tr");

            var td1 = document.createElement("td");
            var thName1 = document.createTextNode("Name");
            td1.append(thName1);
            td1.style.border = "1px solid black";
            td1.style.backgroundColor = "1px solid grey";

            var td2 = document.createElement("td");
            var thMNo = document.createTextNode("MobileNumber");
            td2.append(thMNo);
            td2.style.border = "1px solid black";
            td2.style.backgroundColor = "1px solid grey";

            var td3 = document.createElement("td");
            var thSapId = document.createTextNode("SapId");
            td3.append(thSapId);
            td3.style.border = "1px solid black";

            var td4 = document.createElement("td");
            var thEmail = document.createTextNode("Email");
            td4.append(thEmail);
            td4.style.border = "1px solid black";


            var td5 = document.createElement("td");
            var thGender = document.createTextNode("Gender");
            td5.append(thGender);
            td5.style.border = "1px solid black";

            var td7 = document.createElement("td");
            var action = document.createTextNode("Action");
            td7.append(action);
            td7.style.border = "1px solid black";

            var td6 = document.createElement("td");
            var thId = document.createTextNode("Id");
            td6.append(thId);
            td6.style.border = "1px solid black";

            table.style.border = "1px solid black";

            tr.append(td1);
            tr.append(td2);
            tr.append(td3);
            tr.append(td4);
            tr.append(td5);
            tr.append(td6);
            tr.append(td7);

            thead.appendChild(tr);
            table.appendChild(thead);
            table.appendChild(tbody);


            var data = JSON.parse(this.response);
            var length = data.length;
            if (length > 0) {
                for (var i = 0; i < length; i++) {
                    var tableBody = document.createElement("tr");

                    var td1Body = document.createElement("td");
                    var textNode1 = document.createTextNode(data[i].name);
                    td1Body.append(textNode1);

                    var td2Body = document.createElement("td");
                    var textNode2 = document.createTextNode(data[i].mno);
                    td2Body.append(textNode2);

                    var td3Body = document.createElement("td");
                    var textNode3 = document.createTextNode(data[i].sapId);
                    td3Body.append(textNode3);

                    var td4Body = document.createElement("td");
                    var textNode4 = document.createTextNode(data[i].email);
                    td4Body.append(textNode4);

                    var td5Body = document.createElement("td");
                    var textNode5 = document.createTextNode(data[i].gender);
                    td5Body.append(textNode5);

                    var td6Body = document.createElement("td");
                    var textNode6 = document.createTextNode(data[i].id);
                    td6Body.append(textNode6);


                    var td7Body = document.createElement("td");
                    var uButton = document.createElement("button");
                    var uTextNode = document.createTextNode("Update");

                    uButton.appendChild(uTextNode);
                    var dButton = document.createElement("button");
                    var dTextNode = document.createTextNode("Delete");
                    uButton.addEventListener("click", function () {
                        var data = this.parentElement.parentElement.cells;
                        var obj1 = { name: data[0].innerHTML, mno: data[1].innerHTML, sapId: data[2].innerHTML, email: data[3].innerHTML, gender: data[4].innerHTML, id: data[5].innerHTML }
                        localStorage.setItem("user", JSON.stringify(obj1));
                        window.location.assign("update.html");
                    });
                    dButton.addEventListener("click", function () {
                        var data = this.parentElement.parentElement.cells;
                        var httpRequest;

                        if (window.XMLHttpRequest) {
                            httpRequest = new XMLHttpRequest()
                        } else {
                            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        httpRequest.onreadystatechange = function () {
                            if (this.readyState === 4 && this.status === 200) {
                                confirm("are you sure want to delete record");
                                getData();
                            }
                        }
                        var url = "http://localhost:3000/users/" + data[5].innerHTML;
                        httpRequest.open("delete", url, true);
                        httpRequest.send();
                    })
                    dButton.appendChild(dTextNode);
                    td7Body.append(uButton);
                    td7Body.append(dButton);

                    td1Body.style.border = "1px solid black";
                    td2Body.style.border = "1px solid black";
                    td3Body.style.border = "1px solid black";
                    td4Body.style.border = "1px solid black";
                    td5Body.style.border = "1px solid black";
                    td6Body.style.border = "1px solid black";

                    tableBody.append(td1Body);
                    tableBody.append(td2Body);
                    tableBody.append(td3Body);
                    tableBody.append(td4Body);
                    tableBody.append(td5Body);
                    tableBody.append(td6Body);
                    tableBody.append(td7Body);
                    tbody.append(tableBody);

                }

            } else {
                var h4data = document.createElement("h4");
                var childNode = document.createTextNode("No Data Available in Data base");
                h4data.appendChild(childNode);
                tbody.appendChild(h4data);
            }

            var body = document.getElementsByTagName('body')[0];
            body.appendChild(table);


        }

    }

    httpRequest.open("get", "http://localhost:3000/users", true);
    httpRequest.send();
}

