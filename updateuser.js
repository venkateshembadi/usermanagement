function updateUser() {
    
    var data = JSON.parse(localStorage.getItem("user"));
    document.getElementById("name").value = data.name;
    document.getElementById("mno").value = data.mno;
    document.getElementById("sapId").value = data.sapId;
    document.getElementById("email").value = data.email;
    document.getElementById("gender").value = data.gender;
    function updateUser() {
        var name = document.getElementById("name").value;
        var mno = document.getElementById("mno").value ;
        var sapId = document.getElementById("sapId").value;;
        var email = document.getElementById("email").value ;
        var gender = document.getElementById("gender").nodeValue;
        // var gender;
        var ele = document.getElementsByName('gender');
        for (i = 0; i < ele.length; i++) {
            if (ele[i].checked)
                gender = ele[i].value + " ";
        }
       
        if (name == "") {
            window.alert("Please enter your name.");
            name.focus();
            return false;
        }
        else if (email == "") {
            window.alert("Please enter a valid e-mail address.");
            email.focus();
            return false;
        } 
        else if (sapId == "") {
            window.alert("Please enter a valid sapId.");
            sapId.focus();
            return false;
        } 
        else if (mno == "") {
            window.alert("Please enter a valid mno");
            mno.focus();
            return false;
        } 
        
        var obj = { name: name, mno: mno, sapId: sapId, email: email, gender: gender };
        //var obj={name:name,mno: mno, sapId:sapId,email:email};
        var httpRequest;
        if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest()
        } else {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }
        httpRequest.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                alert("Details updated successfully");
                history.back();
                getData();

            }
        }
        httpRequest.open("put", "http://localhost:3000/users/" + data.id, true);
        httpRequest.setRequestHeader("Content-type", "application/json");
        httpRequest.send(JSON.stringify(obj));

    }

}

